/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package case_3;

import java.awt.Color;
import javax.swing.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;

public class myGUI extends javax.swing.JFrame {
    
    // Default contructor 
    public myGUI() {
        initComponents();
    }
    
    // Menu Panel Creation
    private final JPanel MenuPanel = new JPanel();
    private final JLabel MenuLabel = new JLabel();
    
    // Change Chart Type Label and its contents
    private final JLabel ChangeChartTypeLabel = new JLabel();
    private final ButtonGroup ChartTypeBtnGrp = new ButtonGroup();
    // Bar Chart
    private final JRadioButton BarChartRB = new JRadioButton();
    private final JLabel BarChartIcon = new JLabel();
    //Line Chart
    private final JRadioButton LineChartRB = new JRadioButton();
    private final JLabel LineChartIcon = new JLabel();
    // Area Chart
    private final JLabel AreaChartIcon = new JLabel();
    private final JRadioButton AreaChartRB = new JRadioButton();
    
    // Change Y-Axis and its contents
    private final JLabel ChangeYAxisRB = new JLabel();
    private final JLabel CarManufacturerLabel = new JLabel();
    private final JLabel AdjustTimelineLabel = new JLabel();
    private final JRadioButton VolvoRB = new JRadioButton();
    private final JRadioButton AudiRB = new JRadioButton();

    // Data Type Label and its contents
    private final JLabel DataTypeLabel = new JLabel();
    private final ButtonGroup DataTypeBtnGrp = new ButtonGroup();
    private final JRadioButton QuantitySoldRB = new JRadioButton();
    private final JRadioButton PercentShareRB = new JRadioButton();

    // Save Chart to File Button
    private final JButton SaveChartToFileButton = new JButton();
    
    
    // Adjust Timeline Panel and its contents
    private final JPanel TimeLinePanel = new JPanel();
    private final JLabel TimeLineIcon = new JLabel();
    private final JButton OneMonthButton = new JButton();
    private final JButton SixMonthButton = new JButton();
    private final JButton OneYearButton = new JButton();
    private final JButton FiveYearButton = new JButton();
    private final JButton TenYearLabel = new JButton();
    
    
    //Chart Panel and its contents
    private final JPanel myChartPanel = new JPanel();


    // Key Panel and its contents
    private final JPanel KeyPanel = new JPanel();
    private final JLabel KeyLabel = new JLabel();

    // Side Panel Tools Panel and its contents
    private final JPanel SidePanelTools = new JPanel();
    private final JButton RevertButton = new JButton();
    private final JButton ZoomInButton = new JButton();
    private final JButton ZoomOutButton = new JButton();

    
    // Table Panel and its contents
    private final JPanel TablePanel = new JPanel();
    
    // GUI Code
    private void initComponents() {
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        
        // Menu Panel
        MenuPanel.setBackground(new java.awt.Color(30, 41, 50));
        // Menu Label
        MenuLabel.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        MenuLabel.setForeground(new java.awt.Color(184, 179, 196));
        MenuLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/case_3/icons8-menu-64 (3).png"))); // NOI18N
        MenuLabel.setText("Menu");
        
        
        // Change Chart Type Label
        ChangeChartTypeLabel.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        ChangeChartTypeLabel.setForeground(new java.awt.Color(184, 179, 196));
        ChangeChartTypeLabel.setText("Change Chart Type");
        // Bar Chart
        BarChartRB.setBackground(new java.awt.Color(30, 41, 50));
        ChartTypeBtnGrp.add(BarChartRB);
        BarChartRB.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        BarChartRB.setForeground(new java.awt.Color(184, 179, 196));
        BarChartRB.setText("Bar");
        BarChartRB.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        BarChartIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/case_3/BarChartIcon.png"))); // NOI18N
        BarChartRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BarChartRBActionPerformed(evt);
            }
        });
        // Line Chart
        LineChartRB.setBackground(new java.awt.Color(30, 41, 50));
        ChartTypeBtnGrp.add(LineChartRB);
        LineChartRB.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        LineChartRB.setForeground(new java.awt.Color(184, 179, 196));
        LineChartRB.setText("Line");
        LineChartIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/case_3/icons8-line-chart-64 (1).png"))); // NOI18N
        LineChartRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BarChartRBActionPerformed(evt);
            }
        });
        // Area Chart
        AreaChartRB.setBackground(new java.awt.Color(30, 41, 50));
        ChartTypeBtnGrp.add(AreaChartRB);
        AreaChartRB.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        AreaChartRB.setForeground(new java.awt.Color(184, 179, 196));
        AreaChartRB.setText("Area");
        AreaChartIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/case_3/AreaChartIcon1.png"))); // NOI18N
        AreaChartRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AreaChartRBActionPerformed(evt);
            }
        });
        
        
        // Change Y-Axis
        ChangeYAxisRB.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        ChangeYAxisRB.setForeground(new java.awt.Color(184, 179, 196));
        ChangeYAxisRB.setText("Change Y-Axis");
        // Car Manufacturer Label
        CarManufacturerLabel.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        CarManufacturerLabel.setForeground(new java.awt.Color(184, 179, 196));
        CarManufacturerLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/case_3/icons8-car-40.png"))); // NOI18N
        CarManufacturerLabel.setText("Car Manufacturer");
        // Volvo
        VolvoRB.setBackground(new java.awt.Color(30, 41, 50));
        VolvoRB.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        VolvoRB.setForeground(new java.awt.Color(184, 179, 196));
        VolvoRB.setText("Volvo");
        VolvoRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VolvoRBActionPerformed(evt);
            }
        });
        // Audi
        AudiRB.setBackground(new java.awt.Color(30, 41, 50));
        AudiRB.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        AudiRB.setForeground(new java.awt.Color(184, 179, 196));
        AudiRB.setText("Audi");
        AudiRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VolvoRBActionPerformed(evt);
            }
        });
        
        
        // Data Type Label
        DataTypeLabel.setBackground(new java.awt.Color(30, 41, 50));
        DataTypeLabel.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        DataTypeLabel.setForeground(new java.awt.Color(184, 179, 196));
        DataTypeLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/case_3/icons8-data-transfer-64.png"))); // NOI18N
        DataTypeLabel.setText("Data Type");
        // Quantity Sold Radio Button
        QuantitySoldRB.setBackground(new java.awt.Color(30, 41, 50));
        DataTypeBtnGrp.add(QuantitySoldRB);
        QuantitySoldRB.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        QuantitySoldRB.setForeground(new java.awt.Color(184, 179, 196));
        QuantitySoldRB.setText("Quantity Sold");
        QuantitySoldRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                QuantitySoldRBActionPerformed(evt);
            }
        });
        // Percent Share Radio Button
        PercentShareRB.setBackground(new java.awt.Color(30, 41, 50));
        DataTypeBtnGrp.add(PercentShareRB);
        PercentShareRB.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        PercentShareRB.setForeground(new java.awt.Color(184, 179, 196));
        PercentShareRB.setText("Percent Share");
        PercentShareRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                QuantitySoldRBActionPerformed(evt);
            }
        });

        
        // Save File to Chart Button
        SaveChartToFileButton.setBackground(new java.awt.Color(184, 179, 196));
        SaveChartToFileButton.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        SaveChartToFileButton.setForeground(new java.awt.Color(0, 0, 0));
        SaveChartToFileButton.setText("Save Chart To File");

        
        // Menu Panel and its positioning of contents
        javax.swing.GroupLayout MenuPanelLayout = new javax.swing.GroupLayout(MenuPanel);
        MenuPanel.setLayout(MenuPanelLayout);
        MenuPanelLayout.setHorizontalGroup(
            MenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, MenuPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(MenuLabel)
                .addGap(108, 108, 108))
            .addGroup(MenuPanelLayout.createSequentialGroup()
                .addGroup(MenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(MenuPanelLayout.createSequentialGroup()
                            .addGap(89, 89, 89)
                            .addGroup(MenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(MenuPanelLayout.createSequentialGroup()
                                    .addComponent(BarChartRB)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(BarChartIcon))
                                .addComponent(ChangeChartTypeLabel)
                                .addGroup(MenuPanelLayout.createSequentialGroup()
                                    .addComponent(LineChartRB)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(LineChartIcon))
                                .addGroup(MenuPanelLayout.createSequentialGroup()
                                    .addComponent(AreaChartRB)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(AreaChartIcon))
                                .addComponent(CarManufacturerLabel)))
                        .addGroup(MenuPanelLayout.createSequentialGroup()
                            .addGap(99, 99, 99)
                            .addComponent(ChangeYAxisRB))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, MenuPanelLayout.createSequentialGroup()
                            .addGroup(MenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(MenuPanelLayout.createSequentialGroup()
                                    .addGap(102, 102, 102)
                                    .addComponent(DataTypeLabel))
                                .addGroup(MenuPanelLayout.createSequentialGroup()
                                    .addGap(90, 90, 90)
                                    .addComponent(VolvoRB)
                                    .addGap(18, 18, 18)
                                    .addComponent(AudiRB)))
                            .addGap(19, 19, 19))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, MenuPanelLayout.createSequentialGroup()
                            .addGap(80, 80, 80)
                            .addComponent(SaveChartToFileButton)))
                    .addGroup(MenuPanelLayout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addGroup(MenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(PercentShareRB)
                            .addComponent(QuantitySoldRB))))
                .addContainerGap(84, Short.MAX_VALUE))
        );
        MenuPanelLayout.setVerticalGroup(
            MenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(MenuLabel)
                .addGap(18, 18, 18)
                .addComponent(ChangeChartTypeLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(MenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BarChartRB)
                    .addComponent(BarChartIcon))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(MenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LineChartRB)
                    .addComponent(LineChartIcon))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(MenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(AreaChartRB)
                    .addComponent(AreaChartIcon))
                .addGap(18, 18, 18)
                .addComponent(ChangeYAxisRB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CarManufacturerLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(MenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(VolvoRB)
                    .addComponent(AudiRB))
                .addGap(20, 20, 20)
                .addComponent(DataTypeLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(QuantitySoldRB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PercentShareRB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 87, Short.MAX_VALUE)
                .addComponent(SaveChartToFileButton)
                .addGap(81, 81, 81))
        );
        
        
        // Chart Panel
        myChartPanel.setBackground(new java.awt.Color(23, 0, 79));
        javax.swing.GroupLayout ChartPanelLayout = new javax.swing.GroupLayout(myChartPanel);
        myChartPanel.setLayout(ChartPanelLayout);
        ChartPanelLayout.setHorizontalGroup(
            ChartPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        ChartPanelLayout.setVerticalGroup(
            ChartPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        
        
        // Time Line Panel
        TimeLinePanel.setBackground(new java.awt.Color(30, 41, 50));
        AdjustTimelineLabel.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        AdjustTimelineLabel.setForeground(new java.awt.Color(184, 179, 196));
        AdjustTimelineLabel.setText("Adjust Timeline");
        TimeLineIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/case_3/TimeLineIcon.png"))); // NOI18N
        // 1m
        OneMonthButton.setBackground(new java.awt.Color(184, 179, 196));
        OneMonthButton.setForeground(new java.awt.Color(0, 0, 0));
        OneMonthButton.setText("1m");
        OneMonthButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OneMonthButtonActionPerformed(evt);
            }
        });
        // 6m
        SixMonthButton.setBackground(new java.awt.Color(184, 179, 196));
        SixMonthButton.setForeground(new java.awt.Color(0, 0, 0));
        SixMonthButton.setText("6m");
        SixMonthButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SixMonthButtonActionPerformed(evt);
            }
        });
        // 1y
        OneYearButton.setBackground(new java.awt.Color(184, 179, 196));
        OneYearButton.setForeground(new java.awt.Color(0, 0, 0));
        OneYearButton.setText("1y");
        OneYearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OneYearButtonActionPerformed(evt);
            }
        });
        // 5y
        FiveYearButton.setBackground(new java.awt.Color(184, 179, 196));
        FiveYearButton.setForeground(new java.awt.Color(0, 0, 0));
        FiveYearButton.setText("5y");
        FiveYearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FiveYearButtonActionPerformed(evt);
            }
        });
        // 10y
        TenYearLabel.setBackground(new java.awt.Color(184, 179, 196));
        TenYearLabel.setForeground(new java.awt.Color(0, 0, 0));
        TenYearLabel.setText("10y");
        TenYearLabel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FiveYearButtonActionPerformed(evt);
            }
        });
        
        
        // Time Line Panel and its positioning of contents
        javax.swing.GroupLayout TimeLinePanelLayout = new javax.swing.GroupLayout(TimeLinePanel);
        TimeLinePanel.setLayout(TimeLinePanelLayout);
        TimeLinePanelLayout.setHorizontalGroup(
            TimeLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, TimeLinePanelLayout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addComponent(OneMonthButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56)
                .addComponent(SixMonthButton, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addComponent(OneYearButton, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
                .addComponent(FiveYearButton, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54)
                .addComponent(TenYearLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60))
            .addGroup(TimeLinePanelLayout.createSequentialGroup()
                .addGap(263, 263, 263)
                .addComponent(AdjustTimelineLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TimeLineIcon)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        TimeLinePanelLayout.setVerticalGroup(
            TimeLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, TimeLinePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TimeLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(AdjustTimelineLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TimeLineIcon))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(TimeLinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(OneMonthButton)
                    .addComponent(SixMonthButton)
                    .addComponent(OneYearButton)
                    .addComponent(FiveYearButton)
                    .addComponent(TenYearLabel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        
        // Key Panel and its contents
        KeyPanel.setBackground(new java.awt.Color(30, 41, 50));
        // Key Label
        KeyLabel.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        KeyLabel.setForeground(new java.awt.Color(184, 179, 196));
        KeyLabel.setText("Key");

        
        // Key Panel and its postioning of contents
        javax.swing.GroupLayout KeyPanelLayout = new javax.swing.GroupLayout(KeyPanel);
        KeyPanel.setLayout(KeyPanelLayout);
        KeyPanelLayout.setHorizontalGroup(
            KeyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(KeyPanelLayout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(KeyLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        KeyPanelLayout.setVerticalGroup(
            KeyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(KeyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(KeyLabel)
                .addContainerGap(93, Short.MAX_VALUE))
        );

        
        // Side Panel Tools
        SidePanelTools.setBackground(new java.awt.Color(184, 179, 196));
        // Revert Button
        RevertButton.setBackground(new java.awt.Color(184, 179, 196));
        RevertButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/case_3/RevertIcon.png"))); // NOI18N
        RevertButton.setBorder(null);
        RevertButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RevertButtonActionPerformed(evt);
            }
        });
        // Zoom In Button
        ZoomInButton.setBackground(new java.awt.Color(184, 179, 196));
        ZoomInButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/case_3/ZoomInIcon.png"))); // NOI18N
        ZoomInButton.setBorder(null);
        ZoomInButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ZoomInButtonActionPerformed(evt);
            }
        });
        // Zoom Out Button
        ZoomOutButton.setBackground(new java.awt.Color(184, 179, 196));
        ZoomOutButton.setForeground(new java.awt.Color(184, 179, 196));
        ZoomOutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/case_3/ZoomOutIcon.png"))); // NOI18N
        ZoomOutButton.setBorder(null);
        ZoomOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ZoomOutButtonActionPerformed(evt);
            }
        });

        
        // Side Panel Tools and its positioning of its contents
        javax.swing.GroupLayout SidePanelToolsLayout = new javax.swing.GroupLayout(SidePanelTools);
        SidePanelTools.setLayout(SidePanelToolsLayout);
        SidePanelToolsLayout.setHorizontalGroup(
            SidePanelToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SidePanelToolsLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(SidePanelToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(ZoomOutButton, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(SidePanelToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(ZoomInButton, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
                        .addComponent(RevertButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(35, Short.MAX_VALUE))
        );
        SidePanelToolsLayout.setVerticalGroup(
            SidePanelToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SidePanelToolsLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(RevertButton, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(ZoomInButton)
                .addGap(34, 34, 34)
                .addComponent(ZoomOutButton)
                .addContainerGap(83, Short.MAX_VALUE))
        );

        
        // Table Panel
        TablePanel.setBackground(new java.awt.Color(184, 179, 196));
        
        // Table Panel and its positioning of contents
        javax.swing.GroupLayout TablePanelLayout = new javax.swing.GroupLayout(TablePanel);
        TablePanel.setLayout(TablePanelLayout);
        TablePanelLayout.setHorizontalGroup(
            TablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        TablePanelLayout.setVerticalGroup(
            TablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(MenuPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(TimeLinePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(myChartPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(SidePanelTools, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(KeyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(TablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(TimeLinePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(myChartPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(KeyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(SidePanelTools, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(TablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(MenuPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }

    private void BarChartRBActionPerformed(java.awt.event.ActionEvent evt) {
    }

    private void AreaChartRBActionPerformed(java.awt.event.ActionEvent evt) {
    }

    private void VolvoRBActionPerformed(java.awt.event.ActionEvent evt) {
    }

    private void QuantitySoldRBActionPerformed(java.awt.event.ActionEvent evt) {
        if(QuantitySoldRB.isSelected()){
                    DefaultCategoryDataset dcd = new DefaultCategoryDataset();
        dcd.setValue(90, "Marks", "Mark");
        dcd.setValue(60, "Marks", "Matt");
        dcd.setValue(30, "Marks", "Max");
        dcd.setValue(10, "Marks", "Mary");
        dcd.setValue(100, "Marks", "Mathew");
        
        JFreeChart jChart = ChartFactory.createBarChart("Student Records", "Student Name", "Student Marks", dcd, PlotOrientation.VERTICAL, true, true, false);
        CategoryPlot plot = jChart.getCategoryPlot();
        plot.setRangeGridlinePaint(Color.BLUE);

        
        ChartFrame cf = new ChartFrame("Student Record", jChart ,true);
        cf.setVisible(true);
        cf.setSize(500,500);
        
        ChartPanel cp = new ChartPanel(jChart);
        myChartPanel.removeAll();
        myChartPanel.add(cp);
        myChartPanel.updateUI();
        }
    }

    private void OneMonthButtonActionPerformed(java.awt.event.ActionEvent evt) {
    }

    private void SixMonthButtonActionPerformed(java.awt.event.ActionEvent evt) {
    }

    private void OneYearButtonActionPerformed(java.awt.event.ActionEvent evt) {
    }

    private void FiveYearButtonActionPerformed(java.awt.event.ActionEvent evt) {
    }

    private void RevertButtonActionPerformed(java.awt.event.ActionEvent evt) {
    }
    
    private void ZoomInButtonActionPerformed(java.awt.event.ActionEvent evt) {
    }
    
    private void ZoomOutButtonActionPerformed(java.awt.event.ActionEvent evt) {
    }

}
