/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package New_Case_3;

import static New_Case_3.MyDataVisualization.arraySize;
import static New_Case_3.MyDataVisualization.backFive;
import static New_Case_3.MyDataVisualization.backHalf;
import static New_Case_3.MyDataVisualization.backOne;
import static New_Case_3.MyDataVisualization.backTen;
import static New_Case_3.MyDataVisualization.carData;
import static New_Case_3.MyDataVisualization.dateFormat1;
import static New_Case_3.MyDataVisualization.maxYearMo;
import static New_Case_3.MyDataVisualization.maxYearMoStr;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mark Case
 */
public class MainClass {
    
        /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        int kounter = 0;
       
       maxYearMoStr = "1900-1";
       carData = new ArrayList<String>(); //Initialize ArrayList carData
        
        //Read text file in
        FileReader fr = null;
        try {
            fr = new FileReader("Data.txt"); //Reads in text file
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MyDataVisualization.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedReader reader = new BufferedReader(fr);
        try {
            String line;
            Boolean firstTime = true;
            while((line=reader.readLine())!=null) {
                kounter = kounter + 1;
                if (firstTime) {
                   firstTime = false; 
                   maxYearMoStr = line.substring(0,7);
                } else {
                   Date dateOld = dateFormat1.parse(maxYearMoStr); 
                   Date dateNew = dateFormat1.parse(line.substring(0,7)); 
                   if(dateNew.compareTo(dateOld) > 0) {                      
                        maxYearMoStr = line.substring(0,7); 
                       }  
                }              
                carData.add(line);      
            }
            maxYearMo = dateFormat1.parse(maxYearMoStr);
            Calendar c = Calendar.getInstance();
            c.setTime(maxYearMo);
            c.add(Calendar.DATE, -183);
            backHalf = c.getTime();
            c.setTime(maxYearMo);
            c.add(Calendar.DATE, -365);
            backOne = c.getTime();
            c.setTime(maxYearMo);
            c.add(Calendar.DATE, -1825);
            backFive = c.getTime();
            c.setTime(maxYearMo);
            c.add(Calendar.DATE, -3650);
            backTen = c.getTime();
            c.setTime(maxYearMo);

            arraySize = kounter;
            
            } catch (IOException ex) {
                Logger.getLogger(MyDataVisualization.class.getName()).log(Level.SEVERE, null, ex);
        
        } catch (ParseException ex) {
            
            Logger.getLogger(MyDataVisualization.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MyDataVisualization.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MyDataVisualization.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MyDataVisualization.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MyDataVisualization.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MyDataVisualization().setVisible(true);
            }
        });
    } 
}
