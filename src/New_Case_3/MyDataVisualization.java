package New_Case_3;



import java.awt.Color;

import java.awt.Dimension;

import java.awt.Graphics;

import java.awt.Rectangle;

import java.awt.image.BufferedImage;

import java.io.File;

import java.io.IOException;



import java.text.ParseException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;



import java.util.Date;

import java.util.logging.Level;

import java.util.logging.Logger;

import org.jfree.chart.*;

import org.jfree.chart.plot.CategoryPlot;

import org.jfree.chart.plot.PlotOrientation;

import org.jfree.data.category.DefaultCategoryDataset;

import java.util.Iterator;

import javax.imageio.ImageIO;

import javax.swing.JColorChooser;

import javax.swing.JFileChooser;


import javax.swing.JOptionPane;



import javax.swing.filechooser.FileNameExtensionFilter;

import javax.swing.table.DefaultTableModel;




/**

 *

 * @author Mark Case

 */

public class MyDataVisualization extends javax.swing.JFrame {

    

    // Global Variables

    public static String maxYearMoStr;

    public static Date maxYearMo;

    public static ArrayList<String> carData;

    public static int arraySize;

    public static SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM"); 

    public static Date backHalf;

    public static Date backOne;

    public static Date backFive;

    public static Date backTen;

    private File selFile1;

    public static JFreeChart jchart;

    public Number dateFilter = .5;

    public String[][] tableArray;

    public int numCols = 4;

    public int tableArrCount;



    /**

     * Creates new form MyDataVisualization

     */

    public MyDataVisualization() {

        initComponents();

    }



    // Auto Generated Code

    private void initComponents() {



        changeChartType_btnGrp = new javax.swing.ButtonGroup();

        changeYAxis_btnGrp = new javax.swing.ButtonGroup();

        dataType_BtnGrp = new javax.swing.ButtonGroup();

        menu_pnl = new javax.swing.JPanel();

        menu_label = new javax.swing.JLabel();

        changeChartType_label = new javax.swing.JLabel();

        barRB = new javax.swing.JRadioButton();

        LineRB = new javax.swing.JRadioButton();

        AreaRB = new javax.swing.JRadioButton();

        BarIcon = new javax.swing.JLabel();

        LineIcon = new javax.swing.JLabel();

        AreaIcon = new javax.swing.JLabel();

        changeYAxis_label = new javax.swing.JLabel();

        carManu_label = new javax.swing.JLabel();

        volvoRB = new javax.swing.JRadioButton();

        audiRB = new javax.swing.JRadioButton();

        dataType = new javax.swing.JLabel();

        qtySold = new javax.swing.JRadioButton();

        pctShare = new javax.swing.JRadioButton();

        renderChartBtn = new javax.swing.JButton();

        timeline_pnl = new javax.swing.JPanel();

        sixM = new javax.swing.JButton();

        timeline_lbl = new javax.swing.JLabel();

        oneY = new javax.swing.JButton();

        fiveY = new javax.swing.JButton();

        tenY = new javax.swing.JButton();

        legend_pnl = new javax.swing.JPanel();

        legend_lbl = new javax.swing.JLabel();

        outer_chart_pnl = new javax.swing.JPanel();

        inner_chart_pnl = new javax.swing.JPanel();

        outer_JTable_pnl = new javax.swing.JPanel();

        inner_JTable_pnl = new javax.swing.JPanel();

        jScrollPane1 = new javax.swing.JScrollPane();

        myModel = new javax.swing.JTable();

        tool_pnl = new javax.swing.JPanel();

        editColor = new javax.swing.JButton();

        save_img = new javax.swing.JButton();

        tool_lbl = new javax.swing.JLabel();

        loadTableBtn = new javax.swing.JButton();



        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        setPreferredSize(new Dimension(1900,1000));

        menu_pnl.setBackground(new java.awt.Color(30, 40, 51));



        menu_label.setBackground(new java.awt.Color(174, 189, 196));

        menu_label.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N

        menu_label.setForeground(new java.awt.Color(174, 189, 196));

        menu_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New_Case_3/MenuIcon.png"))); // NOI18N

        menu_label.setText("Menu");



        changeChartType_label.setBackground(new java.awt.Color(174, 189, 196));

        changeChartType_label.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N

        changeChartType_label.setForeground(new java.awt.Color(174, 189, 196));

        changeChartType_label.setText("Change Chart Type");



        changeChartType_btnGrp.add(barRB);

        barRB.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        barRB.setForeground(new java.awt.Color(174, 189, 196));

        barRB.setSelected(true);

        barRB.setText("Bar");

        barRB.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                barRBActionPerformed(evt);

            }

        });



        changeChartType_btnGrp.add(LineRB);

        LineRB.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        LineRB.setForeground(new java.awt.Color(174, 189, 196));

        LineRB.setText("Line");

        LineRB.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                LineRBActionPerformed(evt);

            }

        });



        changeChartType_btnGrp.add(AreaRB);

        AreaRB.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        AreaRB.setForeground(new java.awt.Color(174, 189, 196));

        AreaRB.setText("Area");

        AreaRB.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                AreaRBActionPerformed(evt);

            }

        });



        BarIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New_Case_3/BarChartIcon.png"))); // NOI18N



        LineIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New_Case_3/LineChartIcon.png"))); // NOI18N



        AreaIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New_Case_3/AreaChartIcon1.png"))); // NOI18N



        changeYAxis_label.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N

        changeYAxis_label.setForeground(new java.awt.Color(174, 189, 196));

        changeYAxis_label.setText("Change Y-Axis");



        carManu_label.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        carManu_label.setForeground(new java.awt.Color(174, 189, 196));

        carManu_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New_Case_3/IconCar.png"))); // NOI18N

        carManu_label.setText("Car Manufacturer");



        changeYAxis_btnGrp.add(volvoRB);

        volvoRB.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        volvoRB.setForeground(new java.awt.Color(174, 189, 196));

        volvoRB.setSelected(true);

        volvoRB.setText("Volvo");

        volvoRB.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                volvoRBActionPerformed(evt);

            }

        });



        changeYAxis_btnGrp.add(audiRB);

        audiRB.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        audiRB.setForeground(new java.awt.Color(174, 189, 196));

        audiRB.setText("Audi");

        audiRB.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                audiRBActionPerformed(evt);

            }

        });



        dataType.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        dataType.setForeground(new java.awt.Color(174, 189, 196));

        dataType.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New_Case_3/DataTransferIcon.png"))); // NOI18N

        dataType.setText("Data Type");



        dataType_BtnGrp.add(qtySold);

        qtySold.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        qtySold.setForeground(new java.awt.Color(174, 189, 196));

        qtySold.setSelected(true);

        qtySold.setText("Quantity Sold");

        qtySold.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                qtySoldActionPerformed(evt);

            }

        });



        dataType_BtnGrp.add(pctShare);

        pctShare.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        pctShare.setForeground(new java.awt.Color(174, 189, 196));

        pctShare.setText("Percent Share");

        pctShare.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                pctShareActionPerformed(evt);

            }

        });



        renderChartBtn.setBackground(new java.awt.Color(0, 0, 0));

        renderChartBtn.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N

        renderChartBtn.setForeground(new java.awt.Color(174, 189, 196));

        renderChartBtn.setText("Render Chart");

        renderChartBtn.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                renderChartBtnActionPerformed(evt);

            }

        });



        javax.swing.GroupLayout menu_pnlLayout = new javax.swing.GroupLayout(menu_pnl);

        menu_pnl.setLayout(menu_pnlLayout);

        menu_pnlLayout.setHorizontalGroup(

            menu_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(menu_pnlLayout.createSequentialGroup()

                .addGap(56, 56, 56)

                .addComponent(menu_label)

                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

            .addGroup(menu_pnlLayout.createSequentialGroup()

                .addGroup(menu_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)

                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, menu_pnlLayout.createSequentialGroup()

                        .addGap(0, 38, Short.MAX_VALUE)

                        .addComponent(renderChartBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))

                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, menu_pnlLayout.createSequentialGroup()

                        .addGap(38, 38, 38)

                        .addGroup(menu_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

                            .addGroup(menu_pnlLayout.createSequentialGroup()

                                .addGroup(menu_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)

                                    .addGroup(menu_pnlLayout.createSequentialGroup()

                                        .addComponent(volvoRB)

                                        .addGap(18, 18, 18)

                                        .addComponent(audiRB))

                                    .addGroup(menu_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)

                                        .addComponent(dataType)

                                        .addComponent(pctShare))

                                    .addComponent(qtySold))

                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

                            .addGroup(menu_pnlLayout.createSequentialGroup()

                                .addGroup(menu_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

                                    .addComponent(changeYAxis_label)

                                    .addGroup(menu_pnlLayout.createSequentialGroup()

                                        .addComponent(AreaRB)

                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                                        .addComponent(AreaIcon))

                                    .addGroup(menu_pnlLayout.createSequentialGroup()

                                        .addComponent(barRB)

                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                                        .addComponent(BarIcon))

                                    .addComponent(changeChartType_label)

                                    .addGroup(menu_pnlLayout.createSequentialGroup()

                                        .addComponent(LineRB)

                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                                        .addComponent(LineIcon))

                                    .addComponent(carManu_label))

                                .addGap(0, 0, Short.MAX_VALUE)))))

                .addGap(0, 25, Short.MAX_VALUE))

        );

        menu_pnlLayout.setVerticalGroup(

            menu_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(menu_pnlLayout.createSequentialGroup()

                .addContainerGap()

                .addComponent(menu_label)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                .addComponent(changeChartType_label)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                .addGroup(menu_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

                    .addComponent(barRB)

                    .addComponent(BarIcon))

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                .addGroup(menu_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

                    .addComponent(LineRB, javax.swing.GroupLayout.Alignment.TRAILING)

                    .addComponent(LineIcon, javax.swing.GroupLayout.Alignment.TRAILING))

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                .addGroup(menu_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

                    .addComponent(AreaRB)

                    .addComponent(AreaIcon))

                .addGap(18, 18, 18)

                .addComponent(changeYAxis_label)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                .addComponent(carManu_label)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)

                .addGroup(menu_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)

                    .addComponent(volvoRB)

                    .addComponent(audiRB))

                .addGap(18, 18, 18)

                .addComponent(dataType)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                .addComponent(qtySold)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)

                .addComponent(pctShare)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)

                .addComponent(renderChartBtn)

                .addGap(90, 90, 90))

        );



        timeline_pnl.setBackground(new java.awt.Color(30, 40, 51));



        sixM.setText("6m");

        sixM.setBackground(new Color(83, 80, 212));

        sixM.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                sixMActionPerformed(evt);

            }

        });



        timeline_lbl.setBackground(new java.awt.Color(174, 189, 196));

        timeline_lbl.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N

        timeline_lbl.setForeground(new java.awt.Color(174, 189, 196));

        timeline_lbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New_Case_3/TimeLineIcon.png"))); // NOI18N

        timeline_lbl.setText("Time Line Adjustement");



        oneY.setText("1y");

        oneY.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                oneYActionPerformed(evt);

            }

        });



        fiveY.setText("5y");

        fiveY.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                fiveYActionPerformed(evt);

            }

        });



        tenY.setText("10y");

        tenY.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                tenYActionPerformed(evt);

            }

        });



        javax.swing.GroupLayout timeline_pnlLayout = new javax.swing.GroupLayout(timeline_pnl);

        timeline_pnl.setLayout(timeline_pnlLayout);

        timeline_pnlLayout.setHorizontalGroup(

            timeline_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(timeline_pnlLayout.createSequentialGroup()

                .addGap(135, 135, 135)

                .addComponent(timeline_lbl)

                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

            .addGroup(timeline_pnlLayout.createSequentialGroup()

                .addGap(36, 36, 36)

                .addComponent(sixM, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)

                .addGap(72, 72, 72)

                .addComponent(oneY, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)

                .addGap(89, 89, 89)

                .addComponent(fiveY, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

                .addComponent(tenY, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)

                .addGap(34, 34, 34))

        );

        timeline_pnlLayout.setVerticalGroup(

            timeline_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, timeline_pnlLayout.createSequentialGroup()

                .addContainerGap()

                .addComponent(timeline_lbl)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

                .addGroup(timeline_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)

                    .addComponent(sixM)

                    .addComponent(oneY)

                    .addComponent(fiveY)

                    .addComponent(tenY))

                .addGap(37, 37, 37))

        );



        legend_pnl.setBackground(new java.awt.Color(30, 40, 51));



        legend_lbl.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N

        legend_lbl.setForeground(new java.awt.Color(174, 189, 196));

        legend_lbl.setText("Legend");



        javax.swing.GroupLayout legend_pnlLayout = new javax.swing.GroupLayout(legend_pnl);

        legend_pnl.setLayout(legend_pnlLayout);

        legend_pnlLayout.setHorizontalGroup(

            legend_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(legend_pnlLayout.createSequentialGroup()

                .addGap(54, 54, 54)

                .addComponent(legend_lbl)

                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

        );

        legend_pnlLayout.setVerticalGroup(

            legend_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(legend_pnlLayout.createSequentialGroup()

                .addContainerGap()

                .addComponent(legend_lbl)

                .addContainerGap(78, Short.MAX_VALUE))

        );



        outer_chart_pnl.setBackground(new java.awt.Color(23, 0, 79));



        inner_chart_pnl.setBackground(new java.awt.Color(174, 189, 196));

        inner_chart_pnl.setLayout(new javax.swing.BoxLayout(inner_chart_pnl, javax.swing.BoxLayout.LINE_AXIS));



        javax.swing.GroupLayout outer_chart_pnlLayout = new javax.swing.GroupLayout(outer_chart_pnl);

        outer_chart_pnl.setLayout(outer_chart_pnlLayout);

        outer_chart_pnlLayout.setHorizontalGroup(

            outer_chart_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(outer_chart_pnlLayout.createSequentialGroup()

                .addContainerGap()

                .addComponent(inner_chart_pnl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

                .addContainerGap())

        );

        outer_chart_pnlLayout.setVerticalGroup(

            outer_chart_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(outer_chart_pnlLayout.createSequentialGroup()

                .addContainerGap()

                .addComponent(inner_chart_pnl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

                .addContainerGap())

        );



        outer_JTable_pnl.setBackground(new java.awt.Color(23, 0, 79));



        inner_JTable_pnl.setBackground(new java.awt.Color(174, 189, 196));



        myModel.setModel(new javax.swing.table.DefaultTableModel(

            new Object [][] {

                {null, null, null},

                {null, null, null},

                {null, null, null},

                {null, null, null},

                {null, null, null},

                {null, null, null},

                {null, null, null}

            },

            new String [] {

                "Manufacturer", "Date", "QTY Sold", "PCT Share"

            }

        ) {

            Class[] types = new Class [] {

                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class

            };



            public Class getColumnClass(int columnIndex) {

                return types [columnIndex];

            }

        });

        jScrollPane1.setViewportView(myModel);



        javax.swing.GroupLayout inner_JTable_pnlLayout = new javax.swing.GroupLayout(inner_JTable_pnl);

        inner_JTable_pnl.setLayout(inner_JTable_pnlLayout);

        inner_JTable_pnlLayout.setHorizontalGroup(

            inner_JTable_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(inner_JTable_pnlLayout.createSequentialGroup()

                .addContainerGap()

                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)

                .addContainerGap())

        );

        inner_JTable_pnlLayout.setVerticalGroup(

            inner_JTable_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(inner_JTable_pnlLayout.createSequentialGroup()

                .addContainerGap()

                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)

                .addContainerGap())

        );



        javax.swing.GroupLayout outer_JTable_pnlLayout = new javax.swing.GroupLayout(outer_JTable_pnl);

        outer_JTable_pnl.setLayout(outer_JTable_pnlLayout);

        outer_JTable_pnlLayout.setHorizontalGroup(

            outer_JTable_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(outer_JTable_pnlLayout.createSequentialGroup()

                .addContainerGap()

                .addComponent(inner_JTable_pnl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

                .addContainerGap())

        );

        outer_JTable_pnlLayout.setVerticalGroup(

            outer_JTable_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(outer_JTable_pnlLayout.createSequentialGroup()

                .addContainerGap()

                .addComponent(inner_JTable_pnl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

                .addContainerGap())

        );



        tool_pnl.setBackground(new java.awt.Color(174, 189, 196));



        editColor.setBackground(new java.awt.Color(0, 0, 0));

        editColor.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N

        editColor.setForeground(new java.awt.Color(174, 189, 196));

        editColor.setText("Change Color");

        editColor.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                editColorActionPerformed(evt);

            }

        });



        save_img.setBackground(new java.awt.Color(0, 0, 0));

        save_img.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N

        save_img.setForeground(new java.awt.Color(174, 189, 196));

        save_img.setText("Save Chart To File");

        save_img.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                save_imgActionPerformed(evt);

            }

        });



        tool_lbl.setBackground(new java.awt.Color(0, 0, 0));

        tool_lbl.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N

        tool_lbl.setForeground(new java.awt.Color(0, 0, 0));

        tool_lbl.setText("Tools");



        loadTableBtn.setBackground(new java.awt.Color(0, 0, 0));

        loadTableBtn.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N

        loadTableBtn.setForeground(new java.awt.Color(174, 189, 196));

        loadTableBtn.setText("Load Table");

        loadTableBtn.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                loadTableBtnActionPerformed(evt);

            }

        });



        javax.swing.GroupLayout tool_pnlLayout = new javax.swing.GroupLayout(tool_pnl);

        tool_pnl.setLayout(tool_pnlLayout);

        tool_pnlLayout.setHorizontalGroup(

            tool_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(tool_pnlLayout.createSequentialGroup()

                .addGroup(tool_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

                    .addGroup(tool_pnlLayout.createSequentialGroup()

                        .addGap(57, 57, 57)

                        .addComponent(tool_lbl)

                        .addGap(0, 0, Short.MAX_VALUE))

                    .addGroup(tool_pnlLayout.createSequentialGroup()

                        .addContainerGap()

                        .addGroup(tool_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

                            .addComponent(loadTableBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

                            .addComponent(save_img, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

                            .addComponent(editColor, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))

                .addContainerGap())

        );

        tool_pnlLayout.setVerticalGroup(

            tool_pnlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(tool_pnlLayout.createSequentialGroup()

                .addContainerGap()

                .addComponent(tool_lbl)

                .addGap(57, 57, 57)

                .addComponent(editColor)

                .addGap(28, 28, 28)

                .addComponent(save_img)

                .addGap(30, 30, 30)

                .addComponent(loadTableBtn)

                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

        );



        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());

        getContentPane().setLayout(layout);

        layout.setHorizontalGroup(

            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(layout.createSequentialGroup()

                .addContainerGap()

                .addComponent(menu_pnl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)

                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

                    .addGroup(layout.createSequentialGroup()

                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

                            .addComponent(outer_JTable_pnl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

                            .addComponent(outer_chart_pnl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

                        .addGap(7, 7, 7))

                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()

                        .addComponent(timeline_pnl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))

                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)

                    .addComponent(tool_pnl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

                    .addComponent(legend_pnl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

                .addContainerGap())

        );

        layout.setVerticalGroup(

            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

            .addGroup(layout.createSequentialGroup()

                .addContainerGap()

                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

                    .addComponent(menu_pnl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

                    .addGroup(layout.createSequentialGroup()

                        .addComponent(legend_pnl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                        .addComponent(tool_pnl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

                    .addGroup(layout.createSequentialGroup()

                        .addComponent(timeline_pnl, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                        .addComponent(outer_chart_pnl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                        .addComponent(outer_JTable_pnl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))

                .addContainerGap())

        );



        pack();

    } // End initComponents



    

    //MenuPanelActions

    private void AreaRBActionPerformed(java.awt.event.ActionEvent evt) {

        inner_chart_pnl.removeAll(); // Clears JPanel 

        inner_chart_pnl.updateUI(); // Updates JPanel



    }

    private void barRBActionPerformed(java.awt.event.ActionEvent evt) {

        inner_chart_pnl.removeAll();

        inner_chart_pnl.updateUI();

    }

    private void LineRBActionPerformed(java.awt.event.ActionEvent evt) {

        inner_chart_pnl.removeAll();

        inner_chart_pnl.updateUI();

    } 

    private void pctShareActionPerformed(java.awt.event.ActionEvent evt) {

        inner_chart_pnl.removeAll();

        inner_chart_pnl.updateUI();

    }

    private void qtySoldActionPerformed(java.awt.event.ActionEvent evt) {

        inner_chart_pnl.removeAll();

        inner_chart_pnl.updateUI();

    }

    private void volvoRBActionPerformed(java.awt.event.ActionEvent evt) {                                             

        inner_chart_pnl.removeAll();

        inner_chart_pnl.updateUI();

    } 

    private void audiRBActionPerformed(java.awt.event.ActionEvent evt) {                                             

        inner_chart_pnl.removeAll();

        inner_chart_pnl.updateUI();

    }

    // Renders Chart

    private void renderChartBtnActionPerformed(java.awt.event.ActionEvent evt) {

        RenderIt();

    }

    

    // Render jchart method

    private void RenderIt() {

        String manufacturer = "";

        DefaultCategoryDataset dcd = new DefaultCategoryDataset();     

        String[] splitLine = new String[4];

        String[][] twoD_arr = new String[arraySize][numCols];

        int kount = 0;

        double dnum = 0;

        double dnum2 = 0;

        int inum = 0;

        //String[][] tableArray = new String[arraySize][numCols];

        

        if (audiRB.isSelected()) { // Check what manu is selected

            manufacturer = "Audi";

        }

        else if (volvoRB.isSelected()) {

            manufacturer = "Volvo";       

        }

        

        Iterator<String> itr = carData.iterator(); // Iterate and split data

        while(itr.hasNext()){

            splitLine = itr.next().split(",");

            for(int i=0;i<=3;i++){

                twoD_arr[kount][i] = splitLine[i];

            }

            kount = kount + 1;

        }



        for(int i=0; i< twoD_arr.length; i++){  // Check what data type is selected and timeline adjustment

            if (pctShare.isSelected() && (twoD_arr[i][1]).equals(manufacturer)) {

                dnum = Double.parseDouble(twoD_arr[i][3]);  

                try { 

                    Date dateNew = dateFormat1.parse(twoD_arr[i][0].substring(0,7));

                    Date filterIt;

                    if (dateFilter == (Number).5) {

                        filterIt = backHalf;

                    } else if (dateFilter == (Number)1) {

                        filterIt = backOne;   

                    } else if (dateFilter == (Number)5) {

                        filterIt = backFive;    

                    } else if (dateFilter == (Number)10) {

                        filterIt = backTen;

                    } else {

                        filterIt = backHalf;

                    }

                    if(dateNew.compareTo(filterIt) > 0) {       

                        dcd.setValue(dnum, "Percent Share", twoD_arr[i][0]); // jchart values                 

                    }  

                } catch (ParseException ex) {

                    Logger.getLogger(MyDataVisualization.class.getName()).log(Level.SEVERE, null, ex);

                }

            } 

            else if (qtySold.isSelected() && (twoD_arr[i][1]).equals(manufacturer)) {

                inum = Integer.parseInt(twoD_arr[i][2]);

                try { 

                    Date dateNew = dateFormat1.parse(twoD_arr[i][0].substring(0,7));

                    Date filterIt;

                    if (dateFilter == (Number).5) {

                        filterIt = backHalf;

                    } else if (dateFilter == (Number)1) {

                        filterIt = backOne;   

                    } else if (dateFilter == (Number)5) {

                        filterIt = backFive;    

                    } else if (dateFilter == (Number)10) {

                        filterIt = backTen;

                    } else {

                        filterIt = backHalf;

                    }                    

                    if(dateNew.compareTo(filterIt) > 0) {  

                        dcd.setValue(inum, "Quantity Sold", twoD_arr[i][0]); // jchart values

                    }  

                } catch (ParseException ex) {

                    Logger.getLogger(MyDataVisualization.class.getName()).log(Level.SEVERE, null, ex);

                }

            }                

        }



         // jchart creation        

         if( LineRB.isSelected()){

            jchart = ChartFactory.createLineChart("New Car Sales in Norway", "Date", manufacturer, dcd, PlotOrientation.VERTICAL, true, true, false);

        }

        else if(AreaRB.isSelected()){

            jchart = ChartFactory.createAreaChart("New Car Sales in Norway", "Date", manufacturer, dcd, PlotOrientation.VERTICAL, true, true, false);

        }

        else if(barRB.isSelected()){

            jchart = ChartFactory.createBarChart("New Car Sales in Norway", "Date", manufacturer, dcd, PlotOrientation.VERTICAL, true, true, false);       

        }

        else {

           jchart = null; 

        }

         

        // Add jchart to jpanel 

        ChartPanel chartPanel = new ChartPanel(jchart);

        inner_chart_pnl.removeAll();

        inner_chart_pnl.add(chartPanel);

        inner_chart_pnl.updateUI(); 

        

       

   }

    

    // Build jtable

    private void BuildTable() {

        String manufacturer = "";     

        String[] splitLine = new String[4];

        String[][] twoD_arr = new String[arraySize][numCols];

        int kount = 0;

        double dnum = 0;

        double dnum2 = 0;

        int inum = 0;

        int innerKount = 0;

        tableArray = new String[arraySize][numCols];

        

        if (audiRB.isSelected()) {

            manufacturer = "Audi";

        }

        else if (volvoRB.isSelected()) {

            manufacturer = "Volvo";       

        }

        

        Iterator<String> itr = carData.iterator(); // Iterate and split through data *Same as with render chart*

        while(itr.hasNext()){

            splitLine = itr.next().split(",");

            for(int i=0;i<=3;i++){

                twoD_arr[kount][i] = splitLine[i];

            }

            kount = kount + 1;

        }



        for(int i=0; i< twoD_arr.length; i++){  

            if (pctShare.isSelected() && (twoD_arr[i][1]).equals(manufacturer)) {

                dnum = Double.parseDouble(twoD_arr[i][3]);  

                try { 

                    Date dateNew = dateFormat1.parse(twoD_arr[i][0].substring(0,7));

                    Date filterIt;

                    if (dateFilter == (Number).5) {

                        filterIt = backHalf;

                    } else if (dateFilter == (Number)1) {

                        filterIt = backOne;   

                    } else if (dateFilter == (Number)5) {

                        filterIt = backFive;    

                    } else if (dateFilter == (Number)10) {

                        filterIt = backTen;

                    } else {

                        filterIt = backHalf;

                    }

                    if(dateNew.compareTo(filterIt) > 0) {       

                        for (int j = 0; j < numCols; j++) {

                            tableArray[innerKount][j] = twoD_arr[i][j];

                        }  

                        innerKount = innerKount + 1;

                    }  

                } catch (ParseException ex) {

                    Logger.getLogger(MyDataVisualization.class.getName()).log(Level.SEVERE, null, ex);

                }

            } 

            else if (qtySold.isSelected() && (twoD_arr[i][1]).equals(manufacturer)) {

                inum = Integer.parseInt(twoD_arr[i][2]);

                try { 

                    Date dateNew = dateFormat1.parse(twoD_arr[i][0].substring(0,7));

                    Date filterIt;

                    if (dateFilter == (Number).5) {

                        filterIt = backHalf;

                    } else if (dateFilter == (Number)1) {

                        filterIt = backOne;   

                    } else if (dateFilter == (Number)5) {

                        filterIt = backFive;    

                    } else if (dateFilter == (Number)10) {

                        filterIt = backTen;

                    } else {

                        filterIt = backHalf;

                    }                    

                    if(dateNew.compareTo(filterIt) > 0) {  

                        for (int j = 0; j < numCols; j++) {

                            tableArray[innerKount][j] = twoD_arr[i][j];

                        }

                        innerKount = innerKount + 1;

                    }  

                } catch (ParseException ex) {

                    Logger.getLogger(MyDataVisualization.class.getName()).log(Level.SEVERE, null, ex);

                }

                tableArrCount = innerKount;



            }    

        }  // End For Loop

         tableArrCount = innerKount;

   }

    

        

    // Load data into jtable

    private void loadTableBtnActionPerformed(java.awt.event.ActionEvent evt) {

        // TODO add your handling code here:     

        // Get Data for Display

        BuildTable();

        

        String [][] tblArr;

        tblArr = new String[tableArrCount][numCols];

        

        // Load array with selected data

        for(int i=0; i< tableArrCount; i++){  

             for (int j=0; j< numCols; j++) {

                tblArr[i][j] = tableArray[i][j];

             }

        }



        String[] columnNames = { "Date", "Manufacturer", "QTY Sold", "PCT Share"};    

        DefaultTableModel model =  (DefaultTableModel)myModel.getModel();

        for(int i=0; i< tblArr.length; i++){ 

            model.addRow(tblArr[i]);

        }

        myModel.setModel(model);

        

        System.out.println("END Load Table Button");

    }

    

    

    // TimeLineFunctions

    private void sixMActionPerformed(java.awt.event.ActionEvent evt) {

        

        sixM.setBackground(new Color(83, 80, 212));

        oneY.setBackground(new Color(174,189,196));

        fiveY.setBackground(new Color(174,189,196));

        tenY.setBackground(new Color(174,189,196));

        dateFilter = .5; // 6 months

    }



    private void oneYActionPerformed(java.awt.event.ActionEvent evt) {

        

        oneY.setBackground(new Color(83, 80, 212)); 

        sixM.setBackground(new Color(174,189,196));

        fiveY.setBackground(new Color(174,189,196));

        tenY.setBackground(new Color(174,189,196));

        dateFilter = 1; // One year

    }



    private void fiveYActionPerformed(java.awt.event.ActionEvent evt) {                                     

        fiveY.setBackground(new Color(83, 80, 212));

        dateFilter = 5; // five years

        oneY.setBackground(new Color(174,189,196));

        sixM.setBackground(new Color(174,189,196));

        tenY.setBackground(new Color(174,189,196));

        

    }

    

        private void tenYActionPerformed(java.awt.event.ActionEvent evt) {                                     

        tenY.setBackground(new Color(83, 80, 212));

        dateFilter = 10; // ten years

        oneY.setBackground(new Color(174,189,196));

        fiveY.setBackground(new Color(174,189,196));

        sixM.setBackground(new Color(174,189,196));

    }

    

    // Allow user to edit color of jchart

    private void editColorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editColorActionPerformed

        Color newColor = JColorChooser.showDialog(null, "Choose a color", Color.RED);

        CategoryPlot plot = jchart.getCategoryPlot();

        plot.getRenderer().setSeriesPaint(0, newColor);

        

    }//GEN-LAST:event_editColorActionPerformed

    

    // Save chart out to file

    private void save_imgActionPerformed(java.awt.event.ActionEvent evt) {



        Rectangle rec = inner_chart_pnl.getBounds();

        BufferedImage img = new BufferedImage(rec.width, rec.height, BufferedImage.TYPE_INT_ARGB);

        Graphics g = img.getGraphics();

        inner_chart_pnl.paint(g);

        JFileChooser getFile = new JFileChooser();

                getFile.setCurrentDirectory(new File(System.getProperty("user.home")));

                // Filter files

                FileNameExtensionFilter filter1 = new FileNameExtensionFilter("*.Images", "jpg", "png");

                getFile.addChoosableFileFilter(filter1);

                int res = getFile.showSaveDialog(null);

                if(res == JFileChooser.APPROVE_OPTION) {

            selFile1 = getFile.getSelectedFile();

            String path1 = selFile1.getAbsolutePath();

 

                    System.out.println("1st selFile1 = " + selFile1);

                     try {

              

                        ImageIO.write(img, "png", selFile1);

                        JOptionPane.showMessageDialog(null, "File Saved", "", JOptionPane.INFORMATION_MESSAGE);

                      } catch (IOException ex) {

                        ex.printStackTrace();

                      }

                    

                } // End if

    }



    // Variables declaration - do not modify//GEN-BEGIN:variables

    public static javax.swing.JLabel AreaIcon;

    private javax.swing.JRadioButton AreaRB;

    private javax.swing.JLabel BarIcon;

    private javax.swing.JLabel LineIcon;

    private javax.swing.JRadioButton LineRB;

    public static javax.swing.JRadioButton audiRB;

    private javax.swing.JRadioButton barRB;

    private javax.swing.JLabel carManu_label;

    private javax.swing.ButtonGroup changeChartType_btnGrp;

    private javax.swing.JLabel changeChartType_label;

    private javax.swing.ButtonGroup changeYAxis_btnGrp;

    private javax.swing.JLabel changeYAxis_label;

    private javax.swing.JLabel dataType;

    private javax.swing.ButtonGroup dataType_BtnGrp;

    private javax.swing.JButton editColor;

    public static javax.swing.JButton fiveY;

    private javax.swing.JPanel inner_JTable_pnl;

    static javax.swing.JPanel inner_chart_pnl;

    private javax.swing.JScrollPane jScrollPane1;

    private javax.swing.JLabel legend_lbl;

    private javax.swing.JPanel legend_pnl;

    private javax.swing.JButton loadTableBtn;

    private javax.swing.JLabel menu_label;

    private javax.swing.JPanel menu_pnl;

    private javax.swing.JTable myModel;

    public static javax.swing.JButton oneY;

    private javax.swing.JPanel outer_JTable_pnl;

    private javax.swing.JPanel outer_chart_pnl;

    private javax.swing.JRadioButton pctShare;

    private javax.swing.JRadioButton qtySold;

    private javax.swing.JButton renderChartBtn;

    private javax.swing.JButton save_img;

    public static javax.swing.JButton sixM;

    public static javax.swing.JButton tenY;

    private javax.swing.JLabel timeline_lbl;

    private javax.swing.JPanel timeline_pnl;

    private javax.swing.JLabel tool_lbl;

    private javax.swing.JPanel tool_pnl;

    private javax.swing.JRadioButton volvoRB;

    // End of variables declaration//GEN-END:variables

}



